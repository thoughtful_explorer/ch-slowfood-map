# Slow Food Switzerland Location Extractor
Extracts [Slow Food Switzerland](https://www.slowfood.ch/de/was-wir-tun/netzwerk/cooks-alliance) locations from the official website, and places them into KML format.


## Dependencies
* Python 3.x
    * Simplekml for easily building KML files
    * Beautiful Soup 4.x (bs4) for filtering on script tags
    * Selenium for rendering JS to be able to access geodata associated with each location
    * JSON module for JSON-based geodata
    * Regular expressions module to facilitate gleaning address and coordinate information buried somewhat messily in the map/popup data
* Also of course depends on official [Slow Food Switzerland](https://www.slowfood.ch/de/was-wir-tun/netzwerk/cooks-alliance) website.
