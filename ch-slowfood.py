#!/usr/bin/python3

from bs4 import BeautifulSoup
from selenium import webdriver
import simplekml
import re
import json

#Set filename/path for KML file output
kmlfile = "ch-slowfood.kml"
#Set KML schema name
kmlschemaname = "ch-slowfood"
#Set page URL
pageURL = "https://www.slowfood.ch/de/was-wir-tun/netzwerk/cooks-alliance"

def getsoupinit(url=pageURL):
    #Open page URL
    #openURL = requests.get(url)

    #Start a chromium webdriver with the given page URL...
    #...in this case we need the js to be rendered to get the geodata associated with each location, so Selenium it is
    driver = webdriver.Chrome()
    #Implicitly wait in attempt to ensure the page *really* is loaded
    driver.implicitly_wait(15)
    #Get the html
    driver.get(url)

    #Return soupified HTML
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    return soup

# Example JSON:
    '''
    {
      '0', 
          {
            'lat': 46.201122, 
            'lng': 6.1413947, 
            'id': 19832, 
            'title': 'Abigail Antilen', 
            'tooltip': '\t<div class="info_content rot">\n\t\t<h3><a href="https://www.slowfood.ch/de/chefs/anne-besse-abigail-antilen-josee-veerman">Abigail Antilen </a></h3>\n\t\t<p>Au Grutli<br />Genève (GE)</p>\n\t</div>\n'
          }
    }
    '''

def getlocationjson():
    # For no apparent reason, this would not work here: soup.select("script:nth-of-type(17)")
    # ...so it must be done this way. Even still, it only works sometimes - the implicitly_wait 
    # *maybe* helps capture the desired script tag contents appear more reliably
    #Get the 17th instance of <script> and make it into a string because it's already quite close to valid JSON
    scriptstring = str(getsoupinit().findAll("script")[17])
    #Get the JSON string by splitting on the end and beginning bounds of the location JSON list and grabbing the middle
    jsonstring = re.split('var locations = \[\];',re.split('const locationsObj = ', scriptstring)[1])[0] 
    return json.loads(jsonstring)

# Unescape XML characters like "&amp;" into normal text ("&")
def unescape(str_xml):
    str_xml = str_xml.replace("&amp;", "&")
    str_xml = str_xml.replace("&#039;", "'")
    return str_xml

#Saves a KML file of a given list of locations
def createkml(locations):
    #Initialize kml object
    kml = simplekml.Kml()
    
    #First, go through each of the points in the locations object. The items() function must also be used for reasons I don't entirely understand - Python is treating the value of the JSON we want to get into like a dictionary instead of true JSON despite getlocationjson() returning its value with json.loads(). (See example JSON comment above)
    for location in locations.items():
        #Get the location name by splitting the tooltip field by each of 3 strategic strings (<p>, <br />, and </p>) and selecting the second of those after splitting, and also unescaping XML characters like "&amp;" into normal text ("&")
        locationname = unescape(re.split('<p>|<br />|</p>',location[1]["tooltip"])[1])
        #Get the coordinates
        lat = location[1]["lat"]
        lng = location[1]["lng"]
        #Get the point address (really it's just a city name) by splitting similarly to above and choosing the third result after the split, and also unescaping XML characters like "&amp;" into normal text ("&")
        address = unescape(re.split('<p>|<br />|</p>',location[1]["tooltip"])[2])
        #Create the point name and description in the kml
        point = kml.newpoint(name=locationname,description=address)
        #Finally, add coordinates to the feature
        point.coords=[(lng,lat)]
        #Save the kml file
        kml.save(kmlfile)
createkml(getlocationjson())
